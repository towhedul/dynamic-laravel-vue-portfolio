import adminhome from './components/admin/PublicHome.vue'
import CategoryList from './components/admin/category/List.vue'
import AddCategory from './components/admin/category/New.vue'
import EditCategory from './components/admin/category/Edit.vue'

import PostList from './components/admin/post/List.vue'
import AddPost from './components/admin/post/New.vue'
import EditPost from './components/admin/post/Edit.vue'

import ClientList from './components/admin/client/List.vue'
import AddClient from './components/admin/client/New.vue'
import EditClient from './components/admin/client/Edit.vue'

import ProjectList from './components/admin/project/List.vue'
import AddProject from './components/admin/project/New.vue'
import EditProject from './components/admin/project/Edit.vue'

import StaffList from './components/admin/staff/List.vue'
import AddStaff from './components/admin/staff/New.vue'
import EditStaff from './components/admin/staff/Edit.vue'

import HomePublic from './components/frontend/home.vue'
import HomeContact from './components/frontend/contact.vue'
import AboutPublic from './components/frontend/about.vue'
import BlogPost from './components/frontend/blog/blogPost.vue'
import SinglePost from './components/frontend/blog/singlePost.vue'




export  const routes = [
  { path: '/home', component: adminhome},
  { path: '/category', component: CategoryList},
  { path: '/add-category', component: AddCategory},
  { path: '/edit-category/:id', component: EditCategory},

  { path: '/post', component: PostList},
  { path: '/add-post', component: AddPost},
  { path: '/edit-post/:id', component: EditPost},

  { path: '/client', component: ClientList},
  { path: '/add-client', component: AddClient},
  { path: '/edit-client/:id', component: EditClient},

  { path: '/project', component: ProjectList},
  { path: '/add-project', component: AddProject},
  { path: '/edit-project/:id', component: EditProject},

  { path: '/staff', component: StaffList},
  { path: '/add-staff', component: AddStaff},
  { path: '/edit-staff/:id', component: EditStaff},


  { path: '/', component: HomePublic},
  { path: '/about', component: AboutPublic},
  { path: '/contact', component: HomeContact},
  { path: '/blogPost', component: BlogPost},
  { path: '/single-post/:id', component: SinglePost},
  { path: '/category-post/:id', component: BlogPost},
   
]
