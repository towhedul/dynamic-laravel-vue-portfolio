/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
//vuex 
import Vuex from 'vuex'
Vue.use(Vuex)
import StoreData from './store/store.js'
const store = new Vuex.Store(
  
    StoreData
)

//Vue Router Support
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import {routes} from './routes.js'

const router = new VueRouter({
  mode: 'hash',	
  routes // short for `routes: routes`
})


//V-form support

import { Form, HasError, AlertError } from 'vform'

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//Moment Js Support

import moment from 'moment'
Vue.filter('dateFormat',(arg)=>{
	return moment(arg).format('MMM Do YYYY');
})

Vue.filter('ShortLangth',(value,length,suffix)=>{
    return value.substring(0, length) +  suffix;   
})


//Sweet Alert2 support
window.Swal = Swal;
import Swal from 'sweetalert2'

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

window.Toast = Toast;

//ProgressBar Support

import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
})

//Slider 

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);




// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
/*Vue.component('side-bar', require('./components/frontend/Blog/sidebar.vue').default);*/



const app = new Vue({
    el: '#app',
    router,
    store
});
