export default{
	  state:{
         categories: [],
         posts: [],
         blogposts:[],
         singlepost:[],
         client:[],
         allClient:[],
         staff:[],
         project:[]
	  },
	  getters:{
         getCategories(state){
            return state.categories;
         },
         getPosts(state){
         	return state.posts;
         },
         getBlogPosts(state){
            return state.blogposts;
         },
         getAllSinglePost(state){
            return state.singlepost;
         },
         getAllCategory(state){
            return state.categories;
         },
         getAllClient(state){
            return state.client;
         },
         allClient(state){
            return state.allClient;
         },
         getAllStaff(state){
            return state.staff;
         },
         getAllProject(state){
            return state.project;
         },
         getStaff(state){
            return state.staff;
         },
         allProject(state){
            return state.project;
         }
	  },
	  
	  actions:{
	  	 allCategory(context){
             axios.get('/all-category')
             .then((response)=>{
                 context.commit('getAllCategories',response.data.categories)
             })
	  	 },
	  	 allPosts(context){
            axios.get('/all-post')
            .then((response)=>{
                 context.commit('getAllPost',response.data.posts)
            })
            .catch()
	  	 },
         allBlogPost(context){
            axios.get('/blog-post')
            .then((response)=>{
                context.commit('getAllBlogPost',response.data.blogposts)
            })
            .catch()
         },
         singlePost(context,payload){
            axios.get('/single-post/'+payload)
            .then((response)=>{
                context.commit('getSinglepost',response.data.singlepost)
            })
            .catch()
         },
         getCategory(context){
            axios.get('/category')
            .then((response)=>{
                context.commit('getCategory',response.data.categories)
            })
            .catch()
         },
         categoryPostById(context,payload){
            axios.get('/category-post-byId/'+payload)
            .then((response)=>{
                context.commit('getCategoryPost',response.data.blogposts)
            })
            .catch()
         },
         clientList(context){
            axios.get('/client-list')
            .then((response)=>{
               context.commit('getAllClient',response.data.Client)
            })
            .catch()
         },
         allClient(context){
            axios.get('/client')
            .then((response)=>{
                context.commit('allClient',response.data.client);
            })
         },
         allStaff(context){
            axios.get('/staff-list')
            .then((response)=>{
                 context.commit('allStaff',response.data.staff)
            })
            .catch(()=>{

            })
         },
         getAllClient(context){
            axios.get('/staff')
            .then((response)=>{
                 context.commit('getAllStaff',response.data.staff)
            })
            .catch(()=>{

            })
         },

         allProject(context){
            axios.get('/project-list')
            .then((response)=>{
                 context.commit('allProject',response.data.project)
            })
            .catch(()=>{

            })
         },
         getAllProject(context){
            axios.get('/project')
            .then((response)=>{
               context.commit('getAllProject',response.data.project)
            })
            .catch(()=>{

            })
         }
         

	  },
	  mutations:{
         getAllCategories(state,data){
         	  state.categories = data;
         },
         getAllPost(state,data){
         	state.posts = data;
         },
         getAllBlogPost(state,data){
            state.blogposts = data;
         },
         getSinglepost(state,data){
            state.singlepost = data;
         },
         getCategory(state,data){
            state.categories = data;
         },
         getCategoryPost(state,data){
            state.blogposts = data;
         },
         getAllClient(state,data){
            state.client = data;
         },
         allClient(state,data){
            state.allClient = data;
         },
         allStaff(state,data){
            state.staff = data;
         },
         allProject(state,data){
            state.project = data;
         },
         getAllStaff(state,data){
            state.staff = data;
         },
         getAllProject(state,data){
            state.project = data;
         }
	  }
}