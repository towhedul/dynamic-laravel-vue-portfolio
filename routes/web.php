<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.publicmaster');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blog-post','BlogController@show_post');
Route::get('/single-post/{id}','BlogController@single_post');
Route::get('/category','BlogController@category');
Route::get('/category-post-byId/{id}','BlogController@category_post_byId');
Route::get('/client','BlogController@get_client');
Route::get('/staff','BlogController@get_staff');
Route::get('/project','BlogController@get_project');
/*Route::get('{path}', 'HomeController@index')->where('path','([A-z\d\-\/_]+)?');*/


Route::group(['middleware' => ['auth']], function () {
    //category 
	Route::post('/add-category','CategoryController@store');
	Route::get('/all-category','CategoryController@index');
	Route::get('/delete-category/{id}','CategoryController@destroy');
	Route::get('/edit-category/{id}','CategoryController@edit');
	Route::post('/update-category/{id}','CategoryController@update');

	Route::get('/all-post','PostController@index');
	Route::post('/add-post','PostController@store');
	Route::get('/delete-post/{id}','PostController@destroy');
	Route::get('/edit-post/{id}','PostController@edit');
	Route::post('/update-post/{id}','PostController@update');

	Route::get('/client-list','ClientConteroller@index');
	Route::post('/add-client','ClientConteroller@store');
	Route::get('/edit-client/{id}','ClientConteroller@edit');
	Route::post('/update-client/{id}','ClientConteroller@update');
	Route::get('/delete-client/{id}','ClientConteroller@destroy');

    Route::get('/staff-list','StaffController@index');
    Route::post('/add-staff','StaffController@store');
    
    Route::get('/project-list','ProjectController@index');
    Route::post('/add-project','ProjectController@store');

});




