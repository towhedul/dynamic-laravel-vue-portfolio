<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Image;

class ClientConteroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Client = Client::all();
        return response()->json([
            'Client'=>$Client
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'client_name'=>'required'
            

        ]);


        $client = new Client();

        $strpos=strpos($request->photo,';');
        $substr=substr($request->photo,0,$strpos);
        $ext =explode('/', $substr)[1];
        $name=time().'.'.$ext;
        $image = Image::make($request->photo)->resize(300, 200);
        $uolpad_path=public_path()."/image/client/";
        $image->save($uolpad_path.$name);

        $client->client_name = $request->client_name;
        $client->photo = $name;
        $client->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Client = Client::find($id);
        return response()->json([
            'Client'=>$Client
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);

        if (($request->photo) != $client->photo) {
            $strpos=strpos($request->photo,';');
            $substr=substr($request->photo,0,$strpos);
            $ext =explode('/', $substr)[1];
            $name=time().'.'.$ext;
            $image = Image::make($request->photo)->resize(300, 200);
            $uolpad_path=public_path()."/image/client/";
            $image->save($uolpad_path.$name);

            $photo = $uolpad_path.$client->photo;
            if (file_exists($photo)) {
                  @unlink($photo);
            }

        }else{
             $name=$client->photo;
        }

           $client->client_name = $request->client_name;
           $client->photo = $name;
           $client->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $upload_patch=public_path()."/image/client/";
        $image = $client->photo;
        $photo = $upload_patch.$image;
        if (file_exists($photo)) {
            @unlink($photo);
        }
        $client->delete();
    }
}
