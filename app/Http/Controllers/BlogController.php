<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Client;
use App\Staff;
use App\Project;
use Image;

class BlogController extends Controller
{
    public function show_post(){
        
        $posts =  Post::with('user','category')->get();
        return response()->json([
            'blogposts' => $posts
        ],200);
    }

    public function single_post($id){

    $post = Post::with('user','category')->where('id',$id)->orderBy('id','desc')->first();
		    return response()->json([
		    	'singlepost'=>$post
		    ],200);
    }

    public function category(){

    	$categories = Category::all();
    	return response()->json([
             'categories' => $categories
    	],200);
    }

    public function category_post_byId($id){
       
       $posts = Post::with('user','category')->where('category_id',$id)->get();
           return response()->json([
                 'blogposts' => $posts
           ],200);
    }

    public function get_client(){
        $client = Client::all();
        return response()->json([
           'client'=> $client
        ],200);
    }

    public function get_staff(){
        $staff = Staff::all();
        return response()->json([
             'staff'=>$staff
        ],200);
    }

    public function get_project(){
        $project = Project::all();
        return response()->json([
            'project'=>$project
        ],200);
    }

}
