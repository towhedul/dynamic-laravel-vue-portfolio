<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Image;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user','category')->get();
        return response()->json([
           'posts'=>$posts
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'category_id' =>'required',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);


        $post = new Post();

        $strpos=strpos($request->photo,';');
        $substr=substr($request->photo, 0,$strpos);
        $ext=explode('/', $substr)[1];
        $name=time().'.'.$ext;
        $image = Image::make($request->photo)->resize(300, 200);
        $uolpad_path=public_path()."/image/post/";
        $image->save($uolpad_path.$name);


        $post->title = $request->title;
        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->user_id = Auth::user()->id;
        $post->photo = $name;
        $post->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $post = Post::find($id);
       return response()->json([
          'post'=> $post
       ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' =>'required',
            'description' =>'required',
            'category_id' =>'required',
            'photo'=>'sometimes|required|mimes:jpeg,png,jpg,gif,svg'
            
        ]);

        $post = Post::find($id);

        if (($request->photo) != $post->photo) {
            
            $strpos=strpos($request->photo,';');
            $substr=substr($request->photo, 0,$strpos);
            $ext=explode('/', $substr)[1];
            $name=time().'.'.$ext;
            $image = Image::make($request->photo)->resize(300, 200);
            $uolpad_path=public_path()."/image/post/";
            $image->save($uolpad_path.$name);

            $photo=$uolpad_path.$post->photo;
            if (file_exists($photo)) {
                @unlink($photo);
            }
        }else{
            $name = $post->photo;
        }

            $post->title = $request->title;
            $post->description = $request->description;
            $post->category_id = $request->category_id;
            $post->user_id = Auth::user()->id;
            $post->photo = $name;
            $post->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        $current_photo = $post->photo;
        $photo = public_path('image/post/').$current_photo;
        if(file_exists($photo)) {
            
            @unlink($photo);
        }
        $post->delete();
    }
}
