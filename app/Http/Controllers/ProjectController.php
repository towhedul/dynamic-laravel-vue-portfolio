<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Image;


class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = Project::all();
        return response()->json([
             'project'=>$project
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'project_title' =>'required',
            'description' =>'required',
            
            
        ]);

        $project = new Project();

        $strpos = strpos($request->photo, ';');
        $substr = substr($request->photo, 0,$strpos);
        $ext = explode('/', $substr)[1];
        $name = time().'.'.$ext;
        $image = Image::make($request->photo)->resize(900, 650);
        $upload_path = public_path()."/image/project/";
        $image->save($upload_path.$name);

        $project->project_title = $request->project_title;
        $project->description = $request->description;
        $project->photo = $name;
        $project->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
