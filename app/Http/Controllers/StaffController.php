<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use Image;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = Staff::all();
        return response()->json([
             'staff'=>$staff
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' =>'required',
            'designation' =>'required',
            'photo'=>'required|image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);

        $staff = new Staff();

        $strpos = strpos($request->photo, ';');
        $substr = substr($request->photo, 0,$strpos);
        $exe = explode('/', $substr)[1];
        $name = time().'.'.$exe;
        $image = Image::make($request->photo)->resize(300, 200);
        $upload_path = public_path()."/image/staff/";
        $image->save($upload_path.$name);

        $staff->name = $request->name;
        $staff->designation = $request->designation;
        $staff->photo = $name;
        $staff->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function show(Staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(Staff $staff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Staff $staff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function destroy(Staff $staff)
    {
        //
    }
}
