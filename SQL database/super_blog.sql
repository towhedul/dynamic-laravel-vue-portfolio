-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2020 at 08:39 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `super_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `created_at`, `updated_at`) VALUES
(5, 'Web Design', '2020-01-31 09:25:20', '2020-02-23 11:56:24'),
(8, 'Bootstarp Theam', '2020-01-31 09:25:20', '2020-02-23 11:58:15'),
(17, 'Responsive Theam', '2020-02-26 13:18:12', '2020-02-26 13:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `photo`, `created_at`, `updated_at`) VALUES
(6, 'Metrotheme', '1582745492.png', '2020-02-26 13:31:32', '2020-02-26 13:31:32'),
(7, 'Serenity', '1582745534.png', '2020-02-26 13:32:14', '2020-02-26 13:32:14'),
(8, 'Lumia', '1582745553.png', '2020-02-26 13:32:33', '2020-02-26 13:32:33'),
(9, 'Vasperr', '1582745575.png', '2020-02-26 13:32:55', '2020-02-26 13:32:55'),
(10, 'pallacia', '1582745616.png', '2020-02-26 13:33:36', '2020-02-26 13:33:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_01_31_141951_create_categories_table', 2),
(4, '2020_01_31_142209_create_posts_table', 2),
(5, '2020_02_26_031037_create_clients_table', 3),
(6, '2020_02_28_181210_create_staff_table', 4),
(7, '2020_02_28_181348_create_projects_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `user_id`, `title`, `description`, `photo`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'Consequatur nulla laudantium atque quia et sint alias.', 'Vel iure et rem nesciunt recusandae. Eaque iusto ad et quod ut quam. Nemo esse ea quis dolore.', 'https://lorempixel.com/640/480/?54935', '2020-01-31 09:25:20', '2020-02-03 00:53:30'),
(3, 5, 7, 'Est fuga aliquid animi ipsum non quis a.', 'Ab ex tempora ducimus qui dolor. Nihil ipsa aut assumenda sit expedita. Perspiciatis ut facilis qui porro quis ea.', 'https://lorempixel.com/640/480/?33570', '2020-01-31 09:25:20', '2020-01-31 09:25:20'),
(4, 4, 10, 'Reprehenderit ipsam aspernatur est.', 'Eligendi provident magni autem. Vitae dolorem aut vero qui consequuntur. Minus eligendi ipsum non ipsum quis. Unde eos voluptas libero beatae. Numquam iure quis id ex assumenda quibusdam et.', 'https://lorempixel.com/640/480/?79002', '2020-01-31 09:25:20', '2020-01-31 09:25:20'),
(5, 10, 2, 'Aut dolorem sapiente dolore in neque sunt amet consequatur.', 'Aperiam maxime sed harum ut. Magni voluptatem dolorem earum delectus voluptatem incidunt repellat blanditiis. Quam et sed ut reprehenderit aperiam harum deserunt velit. Accusamus sed reprehenderit est et doloremque aut.', 'https://lorempixel.com/640/480/?85994', '2020-01-31 09:25:20', '2020-01-31 09:25:20'),
(6, 6, 9, 'Velit et est commodi et.', 'Blanditiis voluptatem et quam voluptate aut. At quam nihil dicta voluptatem quia deserunt. Voluptas adipisci dolores possimus culpa autem.', 'https://lorempixel.com/640/480/?43227', '2020-01-31 09:25:20', '2020-01-31 09:25:20'),
(7, 6, 1, 'Libero dolores minima deserunt tenetur nam expedita deserunt.', 'Inventore dolor enim atque laboriosam id iusto ipsam. Officia et sequi amet facere et.', '1580711981.jpeg', '2020-01-31 09:25:20', '2020-02-03 00:39:41'),
(8, 8, 4, 'Id enim corporis voluptatem voluptates officiis.', 'Dolorem dolor ut expedita voluptatem aperiam dolorem. Repudiandae vero magni sit autem. Sapiente assumenda id perspiciatis aut praesentium harum optio. Laborum molestiae laudantium maiores voluptates fugit delectus.', 'https://lorempixel.com/640/480/?89122', '2020-01-31 09:25:20', '2020-01-31 09:25:20'),
(12, 10, 1, 'Web Desigm', 'This is description', '1580711055.jpeg', '2020-02-02 22:03:30', '2020-02-03 00:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_title`, `description`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'E-commerce', 'This is ecommerce project.', '1582921273.jpeg', '2020-02-28 14:21:13', '2020-02-28 14:21:13'),
(2, 'Inventory', 'This is Inventory management system.', '1583090664.jpeg', '2020-03-01 13:24:24', '2020-03-01 13:24:24');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `name`, `designation`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Shamim Hassan', 'CEO', '1582918528.jpeg', '2020-02-28 13:35:28', '2020-02-28 13:35:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shamim Hassan', 'shamim009@gmail.com', NULL, '$2y$10$JNh1rpjnGiAf0mqMxqBSEOSgMHIZY9U8CISpVeQOcRrDJ75QpB/b2', NULL, '2020-01-30 22:12:06', '2020-01-30 22:12:06'),
(2, 'Janelle Hodkiewicz', 'maryam.emard@example.org', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'guMOlRHYjE', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(3, 'Dr. Major Abernathy', 'dax85@example.com', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0QdTQA0uNG', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(4, 'Ocie Balistreri PhD', 'nikki.becker@example.com', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XoGbEYsG8D', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(5, 'Domenick Walsh', 'hessel.armando@example.net', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bpUnVqwUxs', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(6, 'Ed Grimes DVM', 'elisabeth90@example.com', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cPv7QiiofL', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(7, 'Geraldine Reilly', 'katrina36@example.org', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CFeCJkDIlE', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(8, 'Mr. Devin Kunze II', 'david.murazik@example.org', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YjDTNtxs6a', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(9, 'Dr. Forest Schultz', 'lowe.janick@example.org', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'odxI9CiuMd', '2020-01-31 09:14:03', '2020-01-31 09:14:03'),
(10, 'Chance Mueller', 'mcclure.shanelle@example.org', '2020-01-31 09:14:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '84Sp5gcEdJ', '2020-01-31 09:14:03', '2020-01-31 09:14:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
